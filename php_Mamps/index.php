<?php
date_default_timezone_set('Europe/Moscow');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'app/header.php';
?>
<h1 class="textstyle1">Спортивные автомобили</h1>
<?php
       $posts = get_short_posts();
?>
<?php foreach ($posts as $post): ?>
<div class="content_inside">
    <section>
        <a href="app/post.php?post_id=<?= $post ['id'];?>">
        <img src=<?= $post ['image_small'];?> class=image1 alt="Ferrari458" title="Подробное рассмотрение Ferrari 458 с фотографиями"></a>
        <a href="app/post.php?post_id=<?= $post ['id'];?>"><div class="divh4 underline"><?= $post ['title'];?></div></a>
        <?= $post ['post_short'];?>       
    </section>
</div>	
<?php endforeach; ?>

<?php
require_once 'app/footer.php';
?>